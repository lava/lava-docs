#!/bin/sh

set -exu

project_id=48478979

if ! [ -d public ]; then
  # download and uncompress latest artifacts
  wget --continue -O artifacts.zip https://gitlab.com/api/v4/projects/${project_id}/jobs/artifacts/main/download?job=pages
  unzip artifacts.zip
  rm -f artifacts.zip
fi

# update from component being updated, if any
if [ -d public-new ]; then
  for path in public-new/*; do
    name=$(basename "${path}")
    rm -rf public/${name}
    cp -a public-new/${name} public/${name}
  done
fi

# update output from local content
rsync -avp src/ public/
ls -1 public/
